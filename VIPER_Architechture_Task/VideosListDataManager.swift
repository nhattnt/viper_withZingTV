//
//  VideosListDataManager.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 08/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import CoreData

class VideosListLocalDataManager:VideosListLocalDataManagerInputProtocol{
    func retrieveVideosList() throws -> [Video] {
        guard let managedOC = CoreDataStore.managedObjectContext else{
            throw PersistenceError.managedObjectContextNotFound
        }
        
        let request: NSFetchRequest<Video> = NSFetchRequest(entityName: "Video")
        
        return try managedOC.fetch(request)
    }
    
    func saveVideo(id: Int, name: String, eps: String, imgUrl: String, videoUrl: String, des: String, type: String, schedule: String) throws {
        guard let managedOC = CoreDataStore.managedObjectContext else {
            throw PersistenceError.managedObjectContextNotFound
        }
        
        if let newVideo = NSEntityDescription.entity(forEntityName: "Video",
                                                    in: managedOC) {
            let video = Video(entity: newVideo, insertInto: managedOC)
            video.id = Int32(id)
            video.name = name
            video.eps = eps
            video.imgUrl = imgUrl
            video.videoUrl = videoUrl
            video.des = des
            video.type = type
            video.schedule = schedule
            try managedOC.save()
        }
        throw PersistenceError.couldNotSaveObject
    }
}
