//
//  VideosListInteractor.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 08/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import Foundation

class VideosListInteractor: VideosListInteractorInputProtocol{
    weak var presenter: VideosListInteractorOutputProtocol?
    var localDatamanager: VideosListLocalDataManagerInputProtocol?
    var remoteDatamanager: VideosListRemoteDataManagerInputProtocol?
    
    func retrieveVideosList() {
        do{
            if let videosList = try localDatamanager?.retrieveVideosList() {
                let videosModelList = videosList.map() {
                    return VideoModel(id: Int($0.id), name: $0.name!, eps: $0.eps!, videoUrl: $0.videoUrl!, imgUrl: $0.imgUrl!, des: $0.des!, type: $0.type!, schedule: $0.schedule!)
                }
                if  videosModelList.isEmpty {
                    remoteDatamanager?.retrieveVideosList()
                }else{
                    presenter?.didRetrieveVideos(videosModelList)
                }
            } else {
                remoteDatamanager?.retrieveVideosList()
            }
            
        } catch {
            presenter?.didRetrieveVideos([])
        }

    }
}

extension VideosListInteractor: VideosListRemoteDataManagerOutputProtocol {
    func onVideosRetrieved(_ videos: [VideoModel]) {
        presenter?.didRetrieveVideos(videos)
        
        for videoModel in videos {
            do {
                try localDatamanager?.saveVideo(id: videoModel.id, name: videoModel.name, eps: videoModel.eps, imgUrl: videoModel.imgUrl, videoUrl: videoModel.videoUrl, des: videoModel.des, type: videoModel.type, schedule: videoModel.schedule)
            } catch  {
                
            }
        }
    }
    
    func onError() {
        presenter?.onError()
    }
}
