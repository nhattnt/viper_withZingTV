//
//  CollectionViewCell.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 14/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import UIKit

class FilmCollectionCell: UICollectionViewCell {
    @IBOutlet weak var filmImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var epsLabel: UILabel!
    
    
    func set(forVideos video: VideoModel){
        nameLabel?.text = video.name
        epsLabel?.text = video.eps
        let url = URL(string: video.imgUrl)!
        let image_from_url_request: URLRequest = URLRequest(url: url)
        
        NSURLConnection.sendAsynchronousRequest(image_from_url_request, queue: OperationQueue.main, completionHandler: {(response: URLResponse!, data: Data!, error: Error!) -> Void in
            
            if error == nil && data != nil {
                self.filmImageView.image = UIImage(data: data)
            }
        })
    }
}
