//
//  Video+CoreDataProperties.swift
//  
//
//  Created by ArrowTN on 14/09/2017.
//
//

import Foundation
import CoreData


extension Video {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Video> {
        return NSFetchRequest<Video>(entityName: "Video")
    }

    @NSManaged public var des: String?
    @NSManaged public var eps: String?
    @NSManaged public var id: Int32
    @NSManaged public var imgUrl: String?
    @NSManaged public var name: String?
    @NSManaged public var videoUrl: String?
    @NSManaged public var type: String?
    @NSManaged public var schedule: String?

}
